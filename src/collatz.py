#!/usr/bin/env python3
import sys

def main( args ):
  if not args[1].isdigit():
    print( "Value supplied not an integer. Exiting." )
    exit(1)
  longest = [1,1]
  sequence_lengths = {}
  for _ in range(1, int(args[1])+1):
    #print( "Number:   {}".format(_) )
    seq = collatz(_)
    #print( "Seq Length: {}".format(len(seq)) )
    #print( "Seq: {}".format(seq) )
    sequence_lengths[_] = len(seq)
    if len(seq) > longest[1]:
      longest[0] = _
      longest[1] = len(seq)
  print("Value: {}\nLength: {}".format(longest[0], longest[1]))
  #print( sequence_lengths )

def collatz( val ):
  seq = [val]
  while val != 1:
    if val % 2 == 0:
      val /= 2
    else:
      val *= 3
      val += 1
    seq.append(int(val))
  return seq

if __name__ == '__main__':
  #print( sys.argv )
  main(sys.argv)
