///Solution function for Project Euler 1
///  Problem - Find the sum of all multiples of 3 or 5 below a number
pub fn solution( max: u64 ) -> u64 {
  let mut result = 0;
  for i in 0..max {
    if i % 3 == 0 || i % 5 == 0 {
      result += i;
    }
  }
  result
}

#[test]
fn simple_test() {
  assert_eq!( solution(10), 23 );
}
