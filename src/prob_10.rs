///Solution function for Project Euler 10
///  Problem - Sum all primes under a specified number 
///  Example - 2, 3, 5, 7 are the primes below 10.  They sum to 17 
pub fn solution( max: u64 ) -> u64 {
  if max == 0 || max == 1 {
    return 0;
  } else if max == 2 {
    return 2;
  }
  let mut sum: u64 = 0; 
  let primes = sieve_of_eratosthenes( max );
  for val in primes {
    sum += val;
  }
  sum
}

fn sieve_of_eratosthenes( n: u64 ) -> Vec<u64> {
  let mut nums: Vec<bool> = Vec::new();
  for _ in 0..n {
    nums.push(true);
  }
  let mut p = 2; 
  while p*p < nums.len()+1 {
    for i in (2*p..nums.len()).step_by(p) {
      //println!("Bad candidate -> {}", i);
      nums[i] = false;
    }
    p += 1;
  }
  let mut primes: Vec<u64> = Vec::new();
  for i in 2..nums.len() {
    if nums[i] == true {
      primes.push(i as u64);
    }
  }
  primes
}

#[test]
fn simple_test() {
  assert_eq!( solution(10), 17 );
}
