///Solution - Project Euler Problem 11
///In the 20×20 grid below, four numbers along a diagonal line have been
///marked in red.
///
///      08 02 22 97 38 15 00 40 00 75 04 05 07 78 52 12 50 77 91 08 
///      49 49 99 40 17 81 18 57 60 87 17 40 98 43 69 48 04 56 62 00 
///      81 49 31 73 55 79 14 29 93 71 40 67 53 88 30 03 49 13 36 65 
///      52 70 95 23 04 60 11 42 69 24 68 56 01 32 56 71 37 02 36 91 
///      22 31 16 71 51 67 63 89 41 92 36 54 22 40 40 28 66 33 13 80 
///      24 47 32 60 99 03 45 02 44 75 33 53 78 36 84 20 35 17 12 50 
///      32 98 81 28 64 23 67 10 26 38 40 67 59 54 70 66 18 38 64 70 
///      67 26 20 68 02 62 12 20 95 63 94 39 63 08 40 91 66 49 94 21 
///      24 55 58 05 66 73 99 26 97 17 78 78 96 83 14 88 34 89 63 72 
///      21 36 23 09 75 00 76 44 20 45 35 14 00 61 33 97 34 31 33 95 
///      78 17 53 28 22 75 31 67 15 94 03 80 04 62 16 14 09 53 56 92 
///      16 39 05 42 96 35 31 47 55 58 88 24 00 17 54 24 36 29 85 57 
///      86 56 00 48 35 71 89 07 05 44 44 37 44 60 21 58 51 54 17 58 
///      19 80 81 68 05 94 47 69 28 73 92 13 86 52 17 77 04 89 55 40 
///      04 52 08 83 97 35 99 16 07 97 57 32 16 26 26 79 33 27 98 66 
///      88 36 68 87 57 62 20 72 03 46 33 67 46 55 12 32 63 93 53 69 
///      04 42 16 73 38 25 39 11 24 94 72 18 08 46 29 32 40 62 76 36 
///      20 69 36 41 72 30 23 88 34 62 99 69 82 67 59 85 74 04 36 16 
///      20 73 35 29 78 31 90 01 74 31 49 71 48 86 81 16 23 57 05 54 
///      01 70 54 71 83 51 54 69 16 92 33 48 61 43 52 01 89 19 67 48 
///
///The product of these numbers is 26 × 63 × 78 × 14 = 1788696.
///
///What is the greatest product of four adjacent numbers in the same
///direction (up, down, left, right, or diagonally) in the 20×20 grid? 
///
///
///Answer: 678f5d2e1eaa42f04fa53411b4f441ac

pub fn solution_11() -> usize {
  let row1  = vec![08, 02, 22, 97, 38, 15, 00, 40, 00, 75, 04, 05, 07, 78, 52, 12, 50, 77, 91, 08];
  let row2  = vec![49, 49, 99, 40, 17, 81, 18, 57, 60, 87, 17, 40, 98, 43, 69, 48, 04, 56, 62, 00];
  let row3  = vec![81, 49, 31, 73, 55, 79, 14, 29, 93, 71, 40, 67, 53, 88, 30, 03, 49, 13, 36, 65];
  let row4  = vec![52, 70, 95, 23, 04, 60, 11, 42, 69, 24, 68, 56, 01, 32, 56, 71, 37, 02, 36, 91];
  let row5  = vec![22, 31, 16, 71, 51, 67, 63, 89, 41, 92, 36, 54, 22, 40, 40, 28, 66, 33, 13, 80];
  let row6  = vec![24, 47, 32, 60, 99, 03, 45, 02, 44, 75, 33, 53, 78, 36, 84, 20, 35, 17, 12, 50];
  let row7  = vec![32, 98, 81, 28, 64, 23, 67, 10, 26, 38, 40, 67, 59, 54, 70, 66, 18, 38, 64, 70];
  let row8  = vec![67, 26, 20, 68, 02, 62, 12, 20, 95, 63, 94, 39, 63, 08, 40, 91, 66, 49, 94, 21];
  let row9  = vec![24, 55, 58, 05, 66, 73, 99, 26, 97, 17, 78, 78, 96, 83, 14, 88, 34, 89, 63, 72];
  let row10 = vec![21, 36, 23, 09, 75, 00, 76, 44, 20, 45, 35, 14, 00, 61, 33, 97, 34, 31, 33, 95];
  let row11 = vec![78, 17, 53, 28, 22, 75, 31, 67, 15, 94, 03, 80, 04, 62, 16, 14, 09, 53, 56, 92];
  let row12 = vec![16, 39, 05, 42, 96, 35, 31, 47, 55, 58, 88, 24, 00, 17, 54, 24, 36, 29, 85, 57];
  let row13 = vec![86, 56, 00, 48, 35, 71, 89, 07, 05, 44, 44, 37, 44, 60, 21, 58, 51, 54, 17, 58];
  let row14 = vec![19, 80, 81, 68, 05, 94, 47, 69, 28, 73, 92, 13, 86, 52, 17, 77, 04, 89, 55, 40];
  let row15 = vec![04, 52, 08, 83, 97, 35, 99, 16, 07, 97, 57, 32, 16, 26, 26, 79, 33, 27, 98, 66];
  let row16 = vec![88, 36, 68, 87, 57, 62, 20, 72, 03, 46, 33, 67, 46, 55, 12, 32, 63, 93, 53, 69];
  let row17 = vec![04, 42, 16, 73, 38, 25, 39, 11, 24, 94, 72, 18, 08, 46, 29, 32, 40, 62, 76, 36];
  let row18 = vec![20, 69, 36, 41, 72, 30, 23, 88, 34, 62, 99, 69, 82, 67, 59, 85, 74, 04, 36, 16];
  let row19 = vec![20, 73, 35, 29, 78, 31, 90, 01, 74, 31, 49, 71, 48, 86, 81, 16, 23, 57, 05, 54];
  let row20 = vec![01, 70, 54, 71, 83, 51, 54, 69, 16, 92, 33, 48, 61, 43, 52, 01, 89, 19, 67, 48];
  let square = vec![row1, row2, row3, row4, row5, row6, row7, row8, row9, row10, row11,
                    row12, row13, row14, row15, row16, row17, row18, row19, row20];

  solution(square)
  
}

pub fn solution( grid: Vec<Vec<usize>> ) -> usize {
  let mut greatest = 0;
  for y in 0..grid.len() {
    for x in 0..grid[y].len() {
      let local_max = greatest_product_of( &grid, x, y );
      if local_max > greatest {
        greatest = local_max;
      }
    }
  }
  greatest
}

fn greatest_product_of( grid: &Vec<Vec<usize>>, x: usize, y: usize ) -> usize {
  //[(x,y), (x+1,y), (x+2,y), (x+3,y)]
  //[(x,y), (x,y+1), (x,y+2), (x,y+3)]
  //[(x,y), (x+1,y+1), (x+2,y+2), (x+3,y+3)]
  //[(x,y), (x+1,y-1), (x+2,y-2), (x+3,y-3)]
  let mut across: [usize; 4] = [0,0,0,0]; 
  let mut down: [usize; 4] = [0,0,0,0]; 
  let mut diag_one: [usize; 4] = [0,0,0,0]; 
  let mut diag_two: [usize; 4] = [0,0,0,0]; 

  for index in 0..4 {
    if let Some(row) = grid.get(x) {
      if let Some(val) = row.get(y+index) {
        across[index] = *val;
      }
    }
    if let Some(row) = grid.get(x+index) {
      if let Some(val) = row.get(y) {
        down[index] = *val;
      }
    }
    if let Some(row) = grid.get(x+index) {
      if let Some(val) = row.get(y+index) {
        diag_one[index] = *val;
      }
    }
    if let Some(row) = grid.get(x+index) {
      if y >= index { //panic if get negative when subtract 2 usize variables
        if let Some(val) = row.get(y-index) {
          diag_two[index] = *val;
        }
      }
    }
  }
  let product_across = across[0]*across[1]*across[2]*across[3];
  let product_down = down[0]*down[1]*down[2]*down[3];
  let product_diag_one = diag_one[0]*diag_one[1]*diag_one[2]*diag_one[3];
  let product_diag_two = diag_two[0]*diag_two[1]*diag_two[2]*diag_two[3];
  let mut greatest_product = product_across;
  if product_down > greatest_product {
    greatest_product = product_down;
  }
  if product_diag_one > greatest_product {
    greatest_product = product_diag_one;
  }
  if product_diag_two > greatest_product {
    greatest_product = product_diag_two;
  }
  greatest_product
}

#[test]
fn simple_test() {
  let grid = vec![vec![2,2,2,2,2],vec![3,3,3,3,1],vec![4,1,1,1,1],vec![4,1,1,1,1]];
  assert_eq!( solution(grid), 96 );
}
//2 2 2 2 2
//3 3 3 3 1
//4 1 1 1 1 
//4 1 1 1 1
