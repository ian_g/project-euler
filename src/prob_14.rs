///Solution function for Project Euler 10
///
///   Which starting number, under one million, produces the longest collatz chain?
///   Collatz Chain:
///     Even n -> n/2
///     Odd n  -> 3*n + 1
///     The final number is 1
///
///   Answer: 5052c3765262bb2c6be537abd60b305e


pub fn solution( max: usize ) -> usize {
  //slow_solution( max )
  //clever_slow_solution( max )
  clever_fast_solution( max )
}

//fn slow_solution( max: usize ) -> usize {
//  let mut longest = 0;
//  let mut longest_value = 0;
//  for i in 1..max {
//    let chain = collatz( i );
//    if chain.len() > longest {
//      longest = chain.len();
//      longest_value = i;
//      //println!("{} -> {}", i, chain.len());
//    }
//  }
//  longest_value
//}

//fn clever_slow_solution( max: usize ) -> usize {
//  let mut longest = 0;
//  let mut longest_value = 0;
//  let mut chain_lengths = HashMap::new();
//  for i in 1..max {
//    let len = collatz_length_hashmap( &i, &chain_lengths );
//    chain_lengths.insert(i, len);
//    if len > longest {
//      longest = len;
//      //println!("{} -> {}", i, len);
//      longest_value = i;
//    }
//  }
//  //println!("{:?}", chain_lengths);
//  longest_value
//}

fn clever_fast_solution( max: usize ) -> usize {
  let mut longest = 0;
  let mut longest_value = 0;
  let mut chain_lengths = Vec::with_capacity( max );
  chain_lengths.push( 0 ); //Indexes from 0, collatz doesn't
  for i in 1..max {
    let len = collatz_length_vec( &i, &chain_lengths );
    chain_lengths.push( len ); 
    if len > longest {
      longest = len;
      //println!("{} -> {}", i, len);
      longest_value = i;
    }
  }
  longest_value
}

fn collatz_length_vec( start: &usize, previous: &Vec<usize> ) -> usize {
  let mut n = *start;
  let mut len = 1;
  while n != 1 {
    if n % 2 == 0 {
      n = n/2;
    } else {
      n = 3*n + 1;
    }
    len += 1;
    if let Some(n_0) = previous.get(n) { 
      return n_0 + len;
    }
  }
  len
}

//fn collatz_length_hashmap( start: &usize, previous: &HashMap<usize,usize> ) -> usize {
//  let mut n = *start;
//  let mut len = 1;
//  while n != 1 {
//    if n % 2 == 0 {
//      n = n/2;
//    } else {
//      n = 3*n + 1;
//    }
//    len += 1;
//    if let Some(n_0) = previous.get(&n) {
//      return n_0 + len - 1;
//    }
//  }
//  len
//}

//fn collatz( start: usize ) -> Vec<usize> {
//  let mut n = start;
//  let mut chain = vec![n];
//  while n != 1 {
//    if n % 2 == 0 {
//      n = n/2;
//    } else {
//      n = 3*n + 1;
//    }
//    chain.push(n);
//  }
//  chain
//}

#[test]
fn simple_test() {
  assert_eq!( solution(10), 9 );
  assert_eq!( solution(20), 18 );
}
