///Solution function for Project Euler 15
///  Problem - Find the sum of all multiples of 3 or 5 below a number
///   Starting in the top left corner of a 2×2 grid, and only being able to move
///   to the right and down, there are exactly 6 routes to the bottom right
///   corner.
///
///   NOTE - Traveling from one corner to next
///
///   How many such routes are there through a 20×20 grid?
///
///   Answer: 928f3957168ac592c4215dcd04e0b678
extern crate num;

use self::num::BigUint;
use std::convert::From;

pub fn solution( dim: BigUint ) -> BigUint {
  //dim+1 corners each direction
  //dim downs
  //dim across
  //dim*2 moves -> (dim*2)! orderings if each move unique
  //don't care order of downs, same with acrosses -> divide by dim! for each
  let unique_order = factorial(BigUint::from(2 as usize)*dim.clone());
  let order_compensation = factorial(dim.clone());
  unique_order / ( order_compensation.clone()*order_compensation )
}

fn factorial( n: BigUint ) -> BigUint {
  //println!("{}", n);
  if n == BigUint::from(1 as usize) {
    return BigUint::from(1 as usize);
  }
  let previous = factorial(n.clone()-BigUint::from(1 as usize));
  let value = n * previous;
  //println!("{}", value);
  value
}

#[test]
fn simple_test() {
  assert_eq!( solution(BigUint::from(2 as usize)), BigUint::from(6 as usize));
}
