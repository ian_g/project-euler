///Solution function for Project Euler 1
///  Problem - 2^15 = 32768, and its digits sum to 26 (3+2+7+6+8)
///            What's the sum of 2^1000

extern crate num;
extern crate num_traits;

use self::num::BigUint;
use self::num_traits::checked_pow;
 
pub fn solution( exp: usize ) -> BigUint {
  let start = BigUint::from(2 as usize);
  let bignum_pow = checked_pow(start, exp);
  let mut sum = BigUint::from(0 as usize);
  if let Some(mut bignum) = bignum_pow {
    while bignum > BigUint::from(0 as usize) {
      let int_sum = sum + (bignum.clone() % BigUint::from(10 as usize));
      sum = int_sum;
      //let sum = sum + (bignum % BigUint::from(10 as usize));
      let int_bignum = bignum / BigUint::from(10 as usize);
      bignum = int_bignum;
      //bignum = bignum / BigUint::from(10 as usize);
    }
  } 
  sum
}

#[test]
fn simple_test() {
  assert_eq!( solution(15), BigUint::from(26 as usize) );
}
