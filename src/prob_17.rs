///Problem 17
///
/// If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total
/// If all the numbers from 1 to 1000 inclusive were written out in words, how many letters would be used?
/// NOTE: Do not count spaces or hyphens.  342 (three hundred and forty two) contains 23 letters.  115 (one hundred and fifteen) contains 20
///       The use of "and" complies with British usage
extern crate num;

use std::process;
//use self::num::BigUint;
use std::collections::HashMap;

pub fn solution( max: usize ) -> usize {
  let mut total_length = 0;
  for i in 1..(max+1) {
    total_length += length_as_word( &i );
  }
  total_length
}

pub fn length_as_word ( num: &usize ) -> usize {
//pub fn solution ( num: &usize ) -> usize {
  let mut value = *num;
  let mut large_multipliers = vec![ "trillion", "billion", "million", "thousand", "" ];
  let mut word = String::new();
  let mut chunk = value % 1000;
  while value > 0 {
    //println!("{}", chunk);
    if let Some(magnitude) = large_multipliers.pop() {
      under_one_hundred( &chunk, &mut word );
      word += magnitude;
    } else {
      println!("Program maximum exceeded! Exiting.\n  Max: 999,999,999,999,999,999");
      process::exit(1);
    }
    value = value / 1000;
    chunk = value % 1000;
  } 
  //println!("{}", word);
  let mut index = 0;
  for _char in word.chars() {
    index += 1;
  }
  //word.chars().len()
  index
}

fn under_one_hundred (num: &usize, word: &mut String) {
  let mut values = HashMap::new();
  values.insert(1, "one");
  values.insert(2, "two");
  values.insert(3, "three");
  values.insert(4, "four");
  values.insert(5, "five");
  values.insert(6, "six");
  values.insert(7, "seven");
  values.insert(8, "eight");
  values.insert(9, "nine");

  let mut tens = HashMap::new();
  tens.insert(2, "twenty");
  tens.insert(3, "thirty");
  tens.insert(4, "forty");
  tens.insert(5, "fifty");
  tens.insert(6, "sixty");
  tens.insert(7, "seventy");
  tens.insert(8, "eighty");
  tens.insert(9, "ninety");

  let zero_power = *num % 10;
  let first_power = (*num % 100)/10;
  let second_power = (*num % 1000)/100;
  
  //let mut word = String::new();
  if second_power > 0 {
    if let Some(name) = values.get(&second_power) {
      *word += name;
      *word += "hundred";
    } else {
      println!("Invalid number given!");
      process::exit(1);
    }
  }

  if second_power != 0 && (first_power != 0 || zero_power != 0) {
    //println!("{},{}", first_power, zero_power);
    *word += "and";
  }

  if first_power > 1 {
    if let Some(name) = tens.get(&first_power) {
      *word += name;
    }
  } else if first_power == 1 {
    if zero_power == 0 {
      *word += "ten";
    } else if zero_power == 1 {
      *word += "eleven";
    } else if zero_power == 2 {
      *word += "twelve";
    } else if zero_power == 3 {
      *word += "thirteen";
    } else if zero_power == 4 {
      *word += "fourteen";
    } else if zero_power == 5 {
      *word += "fifteen";
    } else if zero_power == 6 {
      *word += "sixteen";
    } else if zero_power == 7 {
      *word += "seventeen";
    } else if zero_power == 8 {
      *word += "eighteen";
    } else { //implied: zero_power == 9
      *word += "nineteen";
    }
  } else { () } //implied: first_power == 0

  if zero_power != 0 && first_power != 1 { 
    if let Some(name) = values.get(&zero_power) {
      *word += name;
    } else {
      println!("Invalid number given - {} - ones place", zero_power);
      process::exit(1);
    }
  }
}

#[test]
fn simple_test() {
  assert_eq!( solution(5), 19 )
}
