///Project Euler Problem 19
///   Given the following
///     - 1 Jan 1900 was a Monday
///     - 28 days February (leap years +1)
///     - 30 days Sept, Apr, Jun, Nov
///     - 31 days Jan, Mar, May, Jul, Aug, Oct, Dec
///     - Leap year if year divisible by 4
///     - No leap year on century unless divisble by 400
///
///   How many Sundays were first of month during 20th century (1 Jan 1901 -> 31 Dec 2000)?
///
use std::collections::HashMap;

pub fn solution() -> usize {
  let mut first_sundays = 0;

  //SUN - 0
  //MON - 1
  //TUE - 2
  //WED - 3
  //THU - 4
  //FRI - 5
  //SAT - 6

  let mut leap_year = false;

  let mut year = 1900;
  let mut mon = 0;
  let mut day = 1; //Day of week 

  while year < 2001 {
    increment_month( &mut year, &mut mon, &mut day, &mut leap_year );
    //print_month( &year, &mon, &day );
    if day == 0 && year > 1900 {
      first_sundays += 1;
    }
  }

  first_sundays

}

//fn print_month( year: &usize, mon: &usize, day: &usize ) {
//  let mut month = HashMap::new();
//  month.insert(0, "JAN");
//  month.insert(1, "FEB");
//  month.insert(2, "MAR");
//  month.insert(3, "APR");
//  month.insert(4, "MAY");
//  month.insert(5, "JUN");
//  month.insert(6, "JUL");
//  month.insert(7, "AUG");
//  month.insert(8, "SEPT");
//  month.insert(9, "OCT");
//  month.insert(10, "NOV");
//  month.insert(11, "DEC");
//
//  let mut dayname = HashMap::new();
//  dayname.insert(0, "Sunday");
//  dayname.insert(1, "Monday");
//  dayname.insert(2, "Tuesday");
//  dayname.insert(3, "Wednesday");
//  dayname.insert(4, "Thursday");
//  dayname.insert(5, "Friday");
//  dayname.insert(6, "Saturday");
//
//  println!("{} - 1 {} {}", day, mon, year);
//  let mname = month.get(mon).unwrap();
//  let dname = dayname.get(day).unwrap();
//  println!("({}) {} - 1 {} {}", day, dname, mname, year);
//  
//}

fn increment_month( year: &mut usize, mon: &mut usize, day: &mut usize, leap_year: &mut bool ) {
  let mut length = HashMap::new();
  length.insert(0, 31);
  length.insert(1, 28);
  length.insert(2, 31);
  length.insert(3, 30);
  length.insert(4, 31);
  length.insert(5, 30);
  length.insert(6, 31);
  length.insert(7, 31);
  length.insert(8, 30);
  length.insert(9, 31);
  length.insert(10, 30);
  length.insert(11, 31);

  //Set leap year
  if *year % 400 == 0 { // 4, 100 implied
    *leap_year = true;
  } else if *year % 100 == 0 && *year % 400 != 0 { // 4 implied
    *leap_year = false;
  } else if *year % 4 == 0 { // !100, !400 implied by previous two
    *leap_year = true;
  } else {  // !4, !100, !400 implied by previous three
    *leap_year = false;
  }

  //Increment day 
  if let Some(days) = length.get( mon ) {
    *day += days;
  }
  if *mon == 1 && *leap_year == true {
    *day += 1;
  }
  *day = *day % 7;

  //Increment month
  *mon += 1;
  *mon = *mon % 12;

  //Increment year
  if *mon == 0 {
    *year += 1;
  }

}

//fn main() {
//
//  println!("Problem 13\n  N/A (it's a list of 100 numbers) -> {}",  prob_13::solution());
//
//}
