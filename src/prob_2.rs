///Solution function for Project Euler 2
///  Problem - Sum each even term less than the max in the Fibonacci sequence
pub fn solution( max: u64 ) -> u64 {
  let mut result = 0;
  let mut fibb_a = 0;
  let mut fibb_b = 1;
  let mut _tmp = 0;
  loop {
    if fibb_b >= max { break }
    if fibb_b % 2 == 0 { result += fibb_b }
    _tmp = fibb_a;
    fibb_a = fibb_b;
    fibb_b = fibb_a + _tmp;
  }
  result
}

#[test]
fn simple_test() {
  assert_eq!( solution(100), 44 );
}
