///Project Euler Problem 20
///
/// 10! = 10 * 9 * 8 ... 3 * 2 * 1 = 3628800
/// 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27
///
/// If 100! instead of 10!, what's the sum of the digits?

extern crate num;

use self::num::BigUint;

pub fn solution( factor: usize ) -> BigUint {
  let mut bignum = factorial( BigUint::from(factor as usize) ); 
  let mut digitsum = BigUint::from(0 as usize);
  let ten = BigUint::from(10 as usize);

  while bignum > BigUint::from(0 as usize) {
    //println!( "{}", bignum );
    digitsum += bignum.clone() % ten.clone();
    bignum = bignum.clone() / ten.clone();
  }
  digitsum
}

fn factorial( factor: BigUint ) -> BigUint {
  if factor == BigUint::from( 0 as usize ) {
    return BigUint::from( 1 as usize );
  }
  factor.clone() * factorial( factor - BigUint::from(1 as usize)  )
}

#[test]
fn simple_test() {
  assert_eq!( solution(10), BigUint::from(27 as usize) );
}
