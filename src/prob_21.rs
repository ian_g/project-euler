///Project Euler Problem 21
///   Let d(n) be the sum of proper divisors of n (numbers less than n which divide evenly into n)
///   If d(a) = b and d(b) = a where a != b, then a and b are an amicable pair and a and b are called amicable numbers
///   
///   E.G. Proper divisors of 220 -> 1, 2, 4, 5, 10, 11, 20, 22, 44, 55, 110 -> d(220) = 284
///        Proper divisors of 284 -> 1, 2, 4, 71, 142 -> d(284) = 220
///
///   Problem: Sum all amicable numbers under 10000
use std::collections::HashMap;

pub fn solution( max: usize ) -> usize {
  let mut d_n = HashMap::new();
  for i in 0..max {
    d_n.insert( i, d(i) );
  }

  let ami = amicable_numbers( d_n );
  //println!("{:?}", ami);
  ami.iter().sum()
}

fn amicable_numbers( proper_divisors: HashMap<usize, usize> ) -> Vec<usize> {
  let mut amicable: Vec<usize> = Vec::new();
  for (key, value) in &proper_divisors {
    if let Some( int ) = proper_divisors.get( value ) {
      if int == key && key != value {
        amicable.push( *key );
      }
    }
  }
  amicable
}

fn d( i: usize ) -> usize {
  let divisors = proper_divisors( &i );
  let sum: usize = divisors.iter().sum();
  sum
}

fn proper_divisors( num: &usize ) -> Vec<usize> {
  let mut bound = 1;
  while bound*bound < *num {
    bound += 1;
  }

  let mut factors: Vec<usize> = vec![1];
  for candidate in 2..bound+1 {
    if *num % candidate == 0 {
      factors.push( candidate );
      if candidate*candidate != *num {
        factors.push( *num/candidate );
      }
    }
  }
  factors
}

#[test]
fn simple_test() {
  assert_eq!( solution(10), 0 );
  assert_eq!( solution(300), 504 );
}
