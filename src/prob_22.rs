///Solution function for Project Euler 22
///  Problem - Alphabetize the given file
///          - Score each name ( position of letter in alphabet is point value )
///          - Multiply name score by position in list
///          - Return sum of scores 
use std::fs::File;
use std::process;
use std::io::prelude::*;
use std::collections::HashMap;

pub fn solution_22() -> usize {
  solution( "p022_names.txt" )
}

pub fn solution( fname: &str ) -> usize {
  let file = File::open( fname );
  let mut file = match file {
    Ok(file) => file,
    Err(e) => {println!("Error opening file:\n{}", e); process::exit(1)},
  };
  let mut contents = String::new();
  match file.read_to_string( &mut contents ) {
    Ok(_) => (),
    Err(e) => {println!("Error reading file:\n{}", e); process::exit(1)},
  };

  let contents_vec: Vec<&str> = contents.split(",").collect(); 
  let mut scores: Vec<usize> = Vec::new();

  let abc_order = abc_heapsort( contents_vec );
  
  for (index, word) in abc_order.iter().enumerate() {
    //println!("{}", word);
    let points = score(word);
    scores.push( points * (index+1) );
  }

  scores.iter().sum()
}

fn abc_heapsort( mut seq: Vec<&str> ) -> Vec<&str> {
  let length = seq.len();
  for index in (0..length+1).rev() {
    build_heap( &mut seq, length, index );
  }

  for index in (0..length).rev() {
    seq.swap(0, index);
    build_heap( &mut seq, index, 0 );
  }
  seq
}

fn build_heap( seq: &mut Vec<&str>, size: usize, root: usize ) {
  let mut largest_locn = root;
  
  let left_locn = 2*largest_locn + 1;
  let right_locn = 2*largest_locn + 2;

  if left_locn < size && child_greater(seq[left_locn], seq[largest_locn]) {
    largest_locn = left_locn;
  }
  if right_locn < size && child_greater(seq[right_locn], seq[largest_locn]) {
    largest_locn = right_locn;
  }

  if root != largest_locn {
    //println!("{:?}", seq);
    seq.swap(root, largest_locn);
    build_heap( seq, size, largest_locn );
  }
}

fn child_greater( child: &str, parent: &str) -> bool {
  let mut letter_scores = HashMap::new();
  //File contains only capital letters
  let mut index = 0;
  for letter in "ABCDEFGHIJKLMNOPQRSTUVWXYZ".chars() { 
    index += 1;
    letter_scores.insert(letter, index);
  }
  letter_scores.insert(' ',0);

  let longer: &str;
  let shorter: &str;
  let child_is: &str;

  if child.len() > parent.len() {
    longer = child;
    shorter = parent;
    child_is = "longer";
  } else {
    longer = parent;
    shorter = child;
    child_is = "shorter";
  }
  let mut shorter_chars = shorter.chars();

  for longer_char in longer.chars() {
    let shorter_char = shorter_chars.next().unwrap_or(' ');
    let longer_score = letter_scores.get(&longer_char).unwrap_or(&0);
    let shorter_score = letter_scores.get(&shorter_char).unwrap_or(&0);
    if longer_score == shorter_score {
      ();
    } else if longer_score > shorter_score && child_is == "longer" {
      return true;
    } else if shorter_score > longer_score && child_is == "shorter" {
      return true;
    } else {
      return false;
    }
  }
  return false;
}

fn score( word: &str ) -> usize {
  let mut letter_scores = HashMap::new();
  //File contains only capital letters
  let mut index = 0;
  for letter in "\"ABCDEFGHIJKLMNOPQRSTUVWXYZ".chars() { 
    letter_scores.insert(letter, index);
    index += 1;
  }
  let mut score = 0;
  for c in word.chars() {
    if let Some(points) = letter_scores.get(&c) {
      score += points;
    } else {
      println!("Unknown character {} ({})", c, word);
      score += 0;
      //println!("Character {} not found in hashmap. Exiting", c);
      //process::exit(1);
    }
  }
  score
}

//#[test]
//fn simple_test() {
//  assert_eq!( solution(10), 23 );
//}
