//Project Euler Problem 23
//
//  Perfect Number - Number for which the sum of proper divisors is the number
//  E.G. 28 = 1 + 2 + 4 + 7 + 14
//
//  A number n is deficient if the sum of its proper divisors is less than n
//  A number n is abundant if the sum of its proper divisors is greater than n
//
//  12 is the smallest abundant number ( 12 < 1 + 2 + 3 + 4 + 6 )
//  24 is therefore the smallest number that can be written as the sum of two 
//    abundant numbers
//  Mathematical analysis shows all integers greater than 28123 can be written 
//    as the sum of two abundant numbers
//  This upper limit cannot be reduced analytically though the greatest number 
//    that cannot be expressed as the sum of two abundant numbers is lower
//
//  Find the sum of all positive integers which cannot be written as the sum of
//    two abundant numbers

//Limit here is 28123 b/c all integers over this value can be sum of two abundant
pub fn solution( lim: usize ) -> usize {
  //Bool vec length of lim + 1 (because zero indexed)
  let mut sums: Vec<bool> = Vec::with_capacity(lim+1);
  for i in 0..lim+1 {
    sums.push(false);
  }

  //Vec of abundant numbers less than limit
  let mut abundant: Vec<usize> = Vec::new();
  for i in 1..lim+1 {
    if is_abundant( &i ) {
      //println!("{} -> {:?}", i, proper_divisors(&i));
      abundant.push(i);
    }
  }
  //println!("{:?}", abundant);

  //Go through pairs of abundant numbers.  At their sum, set bool vec to true
  for index in 0..abundant.len() {
    for index2 in index..abundant.len() {
      let val = abundant[index] + abundant[index2];
      //println!("  {}", val);
      if val <= lim {
        sums[val] = true;
      }
    }
  }
  //println!("{:?}", sums);

  //Iterate vec.
  //  True -> sum of two abundant
  //  False -> not sum of two abundant
  //  Goal: sum of numbers not the sum
  //  Therefore add to sum if false
  let mut non_abundant_sum = 0;
  for val in 0..lim+1 {
    if sums[val] {
      ()
    } else {
      non_abundant_sum += val;
    }
  }
  non_abundant_sum
}

fn is_abundant( val: &usize ) -> bool {
  let divisors = proper_divisors( val );
  let divisor_sum: usize = divisors.iter().sum();
  if divisor_sum > *val {
    return true;
  }
  false
}

fn proper_divisors( num: &usize ) -> Vec<usize> {
    if *num == 1 {
      return vec![];
    } else if *num == 2 {
      return vec![1];
    }
    let mut bound = 1;
    while bound*bound < *num {
      bound += 1;
    }

    //Deal with perfect squares
    let adj_bound: usize;
    if bound*bound == *num {
      adj_bound = bound + 1;
    } else {
      adj_bound = bound;
    }

    let mut factors: Vec<usize> = vec![1];   
    for candidate in 2..adj_bound {
    if *num % candidate == 0 {
      factors.push( candidate );
      if candidate*candidate != *num { 
        factors.push( *num/candidate );
      }
    }
  }
  //println!( "{} -> {:?}", num, factors );
  factors
}

//mod prob_23;
//fn main() {
//  println!("Problem 23\n  {} -> {}", 28123, prob_23::solution(28123));
//}
