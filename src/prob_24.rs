///Project Euler Problem 24
///
///   Permutation - an ordered arrangement of objects
///     E.G. - 3124 is a permutation of 1, 2, 3, and 4
///
///   Lexicographic Order - A list of permutations numerically/alphabetically
///     from lowest to highest
///     E.G. 012, 021, 102, 120, 201, 210 are lexicographic permutations of 0, 
///       1, 2
///
///   What is the millionth lexicographic permutation of 0, 1, 2, 3, 4, 5, 6, 
///     7, 8, 9?

//TODO - Heap's algorithm for enumerate!
use std::process;

pub fn solution( elem: Vec<usize>, locn: usize ) -> usize {

  let permutations = enumerate( elem );
  let integer_permutations = integerize( permutations );
  let ordered = sort( integer_permutations );
  //let ordered = permutations;
  //println!("{:?}", ordered);

  if let Some(perm) = ordered.get(locn-1) {
    return perm.clone();
  }
  println!("Not enough permutations available.  Exiting.");
  process::exit(1);
}

fn sort( seq: Vec<usize> ) -> Vec<usize> {
  //bubblesort( seq )
  heapsort( seq )
}

fn heapsort( mut seq: Vec<usize> ) -> Vec<usize> {
  let length = seq.len();
  for index in (0..length+1).rev() {
    build_heap( &mut seq, length, index );
  }

  for index in (0..length).rev() {
    seq.swap(0, index);
    build_heap( &mut seq, index, 0 );
  }
  seq
}

fn build_heap( seq: &mut Vec<usize>, size: usize, root: usize ) {
  let mut largest = root;
  
  let left = 2*largest + 1;
  let right = 2*largest + 2;

  if left < size && seq[left] > seq[largest] {
    largest = left;
  }
  if right < size && seq[right] > seq[largest] {
    largest = right;
  }

  if root != largest {
    //println!("{:?}", seq);
    seq.swap(root, largest);
    build_heap( seq, size, largest );
  }
}

//fn bubblesort( mut seq: Vec<usize> ) -> Vec<usize> {
//  for posn in (0..seq.len()).rev() {
//    for index in 0..posn {
//      if seq.get(index).unwrap() > 
//        seq.get(index+1).unwrap() {
//        seq.swap(index,index+1);
//      }
//    }
//  }
//  seq
//}

fn integerize( perm: Vec<Vec<usize>> ) -> Vec<usize> {
  let mut integers = Vec::new();
  for permutation in &perm {
    let mut value = 0;
    for element in permutation {
      value *= 10;
      value += element;
    }
    integers.push(value);
  }
  integers
}

fn enumerate( mut elem: Vec<usize> ) -> Vec<Vec<usize>> {
  let mut vec1: Vec<Vec<usize>> = vec![vec![]];
  let mut vec2: Vec<Vec<usize>> = Vec::new(); 
  while elem.len() > 0 {
    let new = elem.pop().unwrap();
    while vec1.len() > 0 {
      let mut old_permutation = vec1.pop().unwrap();
      for i in 0..old_permutation.len()+1 {
      //add to every location in popped permutation, adding to vec2 for each one
        let orig = old_permutation.clone();
        old_permutation.insert(i, new);
        vec2.push(old_permutation);
        old_permutation = orig;
      }
    }
    vec1 = vec2.clone();
    vec2 = Vec::new();
  }
  vec1
}

#[test]
fn simple_test() {
  assert_eq!( solution(vec![0,1,2], 5), 201)
}
