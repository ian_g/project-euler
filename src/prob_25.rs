///Project Euler Problem 25
///
///The Fibonacci sequence is defined by the recurrence relation:
///   F_n = F_{n-1} + F_{n-1} where F_0 = 0 and F_1 = 1
///
///Thus, the first 13 terms are:
///   F_0  = 0
///   F_1  = 1
///   F_2  = 1
///   F_3  = 2
///   F_4  = 3
///   F_5  = 5
///   F_6  = 8
///   F_7  = 13
///   F_8  = 21
///   F_9  = 34
///   F_10 = 55
///   F_11 = 89
///   F_12 = 144
///
///The 12th term - F_12 is the first to contain three digits
///
///What is the index of the first term of the Fibonacci sequence to contain 1000 digits?
extern crate num;
extern crate num_traits;
use self::num::BigUint;
use self::num_traits::pow;

pub fn solution( digits: usize ) -> usize {
  let mut seq: Vec<BigUint> = Vec::new();
  let lim = pow(BigUint::from(10 as usize), digits-1);
    //10^x is a number with x+1 digits
  seq.push( BigUint::from(0 as usize) );
  seq.push( BigUint::from(1 as usize) );
  while seq[seq.len() - 1] < lim {
    let length = seq.len();
    let prev = seq[length-1].clone();
    let prev_prev = seq[length-2].clone();
    seq.push(prev+prev_prev);
  }
  seq.len()-1
}

#[test]
fn simple_test() {
  assert_eq!(solution(3), 12);
}
