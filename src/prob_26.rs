///Project Euler Problem 26
///
///A unit fraction contains 1 in the numerator
///
///The first 10 terms are:
///   1/1 = 1
///   1/2 = 0.5
///   1/3 = 0.(3)
///   1/4 = 0.25
///   1/5 = 0.2
///   1/6 = 0.1(6)
///   1/7 = 0.(142857)
///   1/8 = 0.125
///   1/9 = 0.(1)
///   1/10 = 0.1
///where 0.1(6) means there is a 1-digit recurring cycle
///
///Once can see 1/7 has a 6 digit recurring cycle
///
///Which value of d < 1000 has the longest recurring cycle in its decimal notation?

//Gave up on manual calculation
//Looked at solutions
//https://en.wikipedia.org/wiki/Repeating_decimal
//  Section: Fractions with prime denominators
//    Prime denominator not 2 or 5 produces repeating decimal
//    Length equal to order of 10 mod p
//    Multiplicative order is k where a^k == 1 mod p
//      In this case, a=10, so 10^k = 1 mod p, solve for k
//  There's more to dig up, I'm not going to do so tonight

//NOTE - Non-prime denominators will repeat too if their prime factorization isn't all 2 and 5
//  Their repeats will be shorter than the repeating sequences of primes. So.
//use std::process;
extern crate num;
use self::num::BigUint;
use self::num::pow::pow;

pub fn solution( max: usize ) -> usize {
  let mut longest_repeating = 0;
  let mut longest_repeating_length = 0;
  for i in 1..max {
    //Max cycle i-1 b/c only i-1 remainders
    //println!("Value: {}", i);
    let cycle_length = fermat_solution( &i );
    if cycle_length > longest_repeating_length {
      longest_repeating = i;
      longest_repeating_length = cycle_length;
    }
  }
  longest_repeating
}

fn fermat_solution( i: &usize ) -> usize {
  if *i % 2 == 0 || *i % 5 == 0 {
    return 0;
  }
  for n in 1..*i {
    let ten = BigUint::from(10 as usize);
    let order = pow(ten, n);
    if order.clone() % *i == BigUint::from(1 as usize) {
      //println!("  n: {} -> {} -> {}", n, order.clone(), order.clone() % *i);
      return n
    }
  }
  //println!("  End of loop");
  0 
}
