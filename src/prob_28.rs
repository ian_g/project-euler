///Solution function for Project Euler 28
///
///  Starting with the number 1 and moving right in a clockwise direction, a 5x5 spiral is formed as follows:
///
///             21 22 23 24 25
///             20 07 08 09 10
///             19 06 01 02 11
///             18 05 04 03 12
///             17 16 15 14 13
///
///  It can be verified that the sum of the numbers on the diagonals is 101
///  Problem - What is the sum of the numbers on the diagonal for a 1001x1001 spiral?
use std::process;

pub fn solution( dim: usize ) -> usize {
  diagonal_sum( dim )
}

fn diagonal_sum( val: usize ) -> usize {
  if val % 2 != 1 {
    println!("This function only works for odd numbers");
    process::exit(1);
  }

  if val == 1 {
    return 1;
  }
  let mut diagonal_value = val*val;
  //Four of val^2
  diagonal_value *= 4;
  //Each corner is val-1 less
  //  1st -> no subtractions
  //  2nd -> 1 subtraction
  //  3rd -> 2 subtractions
  //  4th -> 3 subtractions
  //  Total -> 6 subtractions
  let correction = 6*(val-1);
  diagonal_value -= correction;
  diagonal_value += diagonal_sum( val-2 );
  //println!("{} -> {}", val, diagonal_value);
  diagonal_value
}

#[test]
fn simple_test() {
  assert_eq!( solution(5), 101 );
}
