///Solution function for Project Euler 3
///  Problem - Find the largest prime factor of a number
//ToDo - More efficient factorization
pub fn solution( max: u64 ) -> u64 {
  let mut num = max;
  let a_sqrt = approx_sqrt( &num );
  let p_fac = prime_factors( &mut num, &a_sqrt );
  let mut fac = 0;
  for i in &p_fac {
    if *i > fac { fac = *i;  }
  }
  fac
}

fn prime_factors( num: &mut u64, a_sqrt: &u64  ) -> Vec<u64> {
  let mut p_fac: Vec<u64> = Vec::new();
  while *num % 2 == 0 {
    p_fac.push( 2 );
    *num = *num / 2;
  }
  for i in (3..*a_sqrt).step_by(2) {
    if *num % i == 0 { 
      p_fac.push( i );
      *num = *num / i;
    }
  }
  if *num != 1 {
    p_fac.push( *num )
  }
  p_fac
}

fn approx_sqrt( num: &u64 ) -> u64 {
  let mut guess = 1;
  while guess*guess < *num {
    guess += 1;
  }
  guess
}

//Later improvement - don't step through odd numbers, step through primes
//fn primes_less_than( val: u64 ) -> Vec<u64> {
//  let mut primes: Vec<u64> = Vec::new();
//  for i in 1..val+1{
//    if is_prime( &i ) { primes.push( i ); }
//  }
//  primes
//}

#[test]
fn simple_test() {
  assert_eq!( solution(13195), 29 );
  assert_eq!( solution(7), 7 );
}
