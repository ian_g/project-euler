///Project Euler 30
/// 
///   Surprisingly, there are only three numbers that can be written as the
///   sum of the fourth powers of their digits:
///     1634 = 1^4 + 6^4 + 3^4 + 4^4
///     8208 = 8^4 + 2^4 + 0^4 + 8^4
///     9474 = 9^4 + 4^4 + 7^4 + 4^4
///   As 1 = 1^4 is not a sum, it is not included
///   The sum of these numbers is 1634 + 8208 + 9474 = 19316
///
///   Find the sum of all the numbers that can be written as the sum of the 
///   fifth powers of their digits 
/// 
pub fn solution( power: usize ) -> usize {

  let mut digits: Vec<usize> = Vec::new();
  for digit in 0..10 {
    digits.push((digit as usize).pow(power as u32));
  }
  
  //Upper Limit - 
  //9 is largest digit
  //9^pow is largest single-digit value
  //Loose upper limit is one order magnitude up from number where
  //  (#digits)*(9^pow) < number
  let max_digit = digits[digits.len()-1];
  let mut upper_limit = 10;
  let mut upper_limit_len = 2;

  while max_digit*upper_limit_len > upper_limit {
    upper_limit *= 10;
    upper_limit_len += 1;
  }

  upper_limit = (upper_limit_len-1)*max_digit;

  //println!( "Upper limit: {}", upper_limit );
  
  let mut num_is_digitsum = Vec::new();

  for i in 10..upper_limit+1 {
    let mut val = i;
    let mut sum = 0;
    while val > 0 {
      sum += (val % 10).pow(power as u32);
      //sum += digits[val%10]; //slower than just multiplying anyway
      val = val / 10;
    }
    if sum == i {
      num_is_digitsum.push(i);
    }
  }

  num_is_digitsum.iter().sum()

  //Combinations of digits - replace as needed
  //See if digits arrange to sum
}

#[test]
fn simple_test() {
  assert_eq!( solution(4), 19316 );
}

//mod prob_30;
//fn main() {
//    println!("Problem 30\n  {} -> {}\n", 5, prob_30::solution(5));
//}
