///Project Euler 31
/// 
///  In England the currency is made up of pound and pence
///  There are 8 coins in circulation:
///    1p, 2p, 5p, 10p, 20p, 50p, £1 (100p), £2 (200p)
/// 
///  How many different ways can £2 be made with any number of coins?
/// 
use std::collections::HashMap;

pub fn solution( value: usize ) -> usize {
  //Generator functions
  //  1p, 2p, 10p, 20p, 50p, 100p, 200p
  let mut generator_1p = HashMap::new();
  let mut generator_2p = HashMap::new();
  let mut generator_5p = HashMap::new();
  let mut generator_10p = HashMap::new();
  let mut generator_20p = HashMap::new();
  let mut generator_50p = HashMap::new();
  let mut generator_100p = HashMap::new();
  let mut generator_200p = HashMap::new();
  for val in 0..value+1 {
    //Coin value, 1 way to generate it
    generator_1p.insert(val,1);
    if val % 2 == 0 {
      generator_2p.insert(val,1);
    }
    if val % 5 == 0 {
      generator_5p.insert(val,1);
    }
    if val % 10 == 0 {
      generator_10p.insert(val,1);
    }
    if val % 20 == 0 {
      generator_20p.insert(val,1);
    }
    if val % 50 == 0 {
      generator_50p.insert(val,1);
    }
    if val % 100 == 0 {
      generator_100p.insert(val,1);
    }
    if val % 200 == 0 {
      generator_200p.insert(val,1);
    }
  }

  //Multiply together (oh god)
  let mut generator = HashMap::new();
  for (val1,mag1) in generator_1p {
    for (val2,mag2) in generator_2p.clone() {
      let newmag = mag1*mag2;
      let newval = val1+val2;
      let generator_term = generator.entry(newval).or_insert(0);
      *generator_term += newmag;
    }
  }
  let tmp = generator.clone();
  generator = HashMap::new();
  for (val1,mag1) in tmp {
    for (val2,mag2) in generator_5p.clone() {
      let newmag2 = mag1*mag2;
      let newval2 = val1+val2;
      let generator_term = generator.entry(newval2).or_insert(0);
      *generator_term += newmag2;
    }
  }
  let tmp = generator.clone();
  generator = HashMap::new();
  for (val1,mag1) in tmp {
    for (val2,mag2) in generator_10p.clone() {
      let newmag2 = mag1*mag2;
      let newval2 = val1+val2;
      let generator_term = generator.entry(newval2).or_insert(0);
      *generator_term += newmag2;
    }
  }
  let tmp = generator.clone();
  generator = HashMap::new();
  for (val1,mag1) in tmp {
    for (val2,mag2) in generator_20p.clone() {
      let newmag2 = mag1*mag2;
      let newval2 = val1+val2;
      let generator_term = generator.entry(newval2).or_insert(0);
      *generator_term += newmag2;
    }
  }
  let tmp = generator.clone();
  generator = HashMap::new();
  for (val1,mag1) in tmp {
    for (val2,mag2) in generator_50p.clone() {
      let newmag2 = mag1*mag2;
      let newval2 = val1+val2;
      let generator_term = generator.entry(newval2).or_insert(0);
      *generator_term += newmag2;
    }
  }
  let tmp = generator.clone();
  generator = HashMap::new();
  for (val1,mag1) in tmp {
    for (val2,mag2) in generator_100p.clone() {
      let newmag2 = mag1*mag2;
      let newval2 = val1+val2;
      let generator_term = generator.entry(newval2).or_insert(0);
      *generator_term += newmag2;
    }
  }
  let tmp = generator.clone();
  generator = HashMap::new();
  for (val1,mag1) in tmp {
    for (val2,mag2) in generator_200p.clone() {
      let newmag2 = mag1*mag2;
      let newval2 = val1+val2;
      let generator_term = generator.entry(newval2).or_insert(0);
      *generator_term += newmag2;
    }
  }

  //println!("{:?}", generator);
  
  if let Some(ways) = generator.get(&value) {
    return *ways;
  }
  println!("No combinations with desired value.");
  0
}

//#[test]
//fn simple_test() {
//  assert_eq!( solution(4), 19316 );
//}

//mod prob_31;
//fn main() {
//    let val = 10;
//    println!("Problem 31\n  {} -> {}\n", val, prob_31::solution(val));
//}
