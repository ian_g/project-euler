///Project Euler Problem 32
///
///   Permutation - an ordered arrangement of objects
///     E.G. - 3124 is a permutation of 1, 2, 3, and 4
///
///   An n-digit number is pandigital if it makes use of all digits from
///   1 to n exactly once.  For example, 15243
///
///   The product 7254 is unusual as 39 * 186 = 7254.  The full expression
///   contains the digits 1 to 9 and is pandigital
///
///   What is the sum of all products that can be written as a pandigital
///   expression?
use std::process;
use std::collections::HashMap;

pub fn solution( elem: Vec<usize> ) -> usize {
  let mut pandigital_products = HashMap::new();
  //Permute elements
  let orderings = permute( elem );
  //println!("{:?}", orderings);
  //look at locations for * and =
  for seq in orderings {
    //println!( "{:?}", seq );
    let products = pandigital_expressions(seq);
    for prod in products {
      //.or_insert(whatever) b/c only doing this to prevent duplicates
      pandigital_products.entry(prod).or_insert(1);
    }
  }
  let mut pandigital_product_sum = 0;
  for (value, _) in pandigital_products {
    pandigital_product_sum += value;
  }
  pandigital_product_sum
}

fn sum_to_length( len: &usize ) -> Vec<Vec<usize>> {
  let mut sequences = Vec::new();
  for i in 1..*len-2 {
    for j in 1..*len-2-i {
      sequences.push( vec![i,j,*len-i-j] );
    }
  }
  sequences
}

fn pandigital_expressions( seq: Vec<usize> ) -> Vec<usize> {
  let mut pandigital_products = Vec::new();
  let mut val1: usize;
  let mut val2: usize;
  let mut val3: usize;
  //Yes, values added backwards. The reverse permutation gives reverse values
  //Yes, multiple of the same product will be returned over may sequences

  let sums = sum_to_length( &seq.len() );
  for expr in sums { 
    val1 = 0;
    val2 = 0;
    val3 = 0;
    for i in 0..expr[0] {
      val1 *= 10;
      val1 += seq[i];
    }
    for i in expr[0]..(expr[0]+expr[1]) {
      val2 *= 10;
      val2 += seq[i];
    }
    for i in (expr[0]+expr[1])..seq.len() {
      val3 *= 10;
      val3 += seq[i];
    }
    if val1 * val2 == val3 {
      pandigital_products.push(val3);
    }
    if val1 * val3 == val2 {
      pandigital_products.push(val2);
    }
    if val2 * val3 == val1 {
      pandigital_products.push(val1);
    }
  }
  pandigital_products
}

fn permute( mut elem: Vec<usize> ) -> Vec<Vec<usize>> {
  let x = elem.pop().unwrap_or_else(|| {
    println!("Empty vector.  Shouldn't happen");
    process::exit(1);
    }
  );
  if elem.len() == 0 {
    return vec![vec![x]];
  }

  let seed_permutations = permute( elem );
  let mut permutations = Vec::new();
  for ordering in seed_permutations {
    for i in 0..ordering.len()+1 {
      let mut new_ordering = ordering.clone();
      new_ordering.insert(i, x);
      permutations.push(new_ordering);
    }
  }
  permutations
}
//mod prob_32;
//fn main() {
//    let val = vec![1,2,3,4,5,6,7,8,9];
//    println!( "Problem 32\n  {:?} -> {}\n", val.clone(), prob_32::solution(val) );
//}
