///Project Euler Problem 33
///
///   The fraction 49/98 is a curious fraction, as an inexperienced
///   mathematician may incorrectly believe a valid simplification is
///   49/98 = 4/8 (which is correct) is obtained by cancelling the 9s.
///
///   We shall consider fractions like 30/50 = 3/5 to be trivial examples.
///
///   There are exactly four non-trivial examples of this type of fraction
///   which are less than one in value and containing two digits in both
///   numerator and denominator.
///
///   If the product of these four fractions is given in its lowest common
///   terms, find the value of the denominator.
use std::collections::HashMap;

pub fn solution() -> usize {
  let mut fractions: Vec<RationalFraction> = Vec::new();
  for d in 10..100 {
    for n in 10..d {
      let frac = RationalFraction::new(n, d);
      if frac.double_zeroes() {
        //println!( "{:?}", frac );
        continue;
      }
      if frac.incorrect_cancellation() {
        fractions.push( frac );
      }
    }
  }

  //Process Fractions now ...

  let mut product_numerator = 1;
  let mut product_denominator = 1;
  for frac in fractions {
    //println!( "{:?}", frac );
    product_numerator *= frac.n;
    product_denominator *= frac.d;
  }
  let product = RationalFraction::new( product_numerator, product_denominator );
  //println!( "{:?}", product );

  //Prime factorization of n + d
  let prime_n = prime_factors( &product.n );
  let prime_d = prime_factors( &product.d );
  //println!( "num -> {:?}\nden -> {:?}", prime_n, prime_d );

  //Ignore parts of d also in n
  let mut reduced_d: HashMap<usize, u32> = HashMap::new();
  for (value, magnitude) in prime_d {
    let magnitude_n = prime_n.get(&value).unwrap_or(&0);
    let reduced_magnitude: u32;
    if magnitude - *magnitude_n < 0 {
      reduced_magnitude = 0;
    } else {
      reduced_magnitude = magnitude as u32 - *magnitude_n as u32;
    }
    reduced_d.insert(value, reduced_magnitude);
  }

  //Return minimized piece
  let mut min_denominator = 1;
  //println!( "reduced -> {:?}", reduced_d );
  for (value, magnitude) in reduced_d {
    //println!( "{}*{}={}", value, magnitude, value.pow(magnitude) );
    min_denominator *= value.pow(magnitude);
  }
  min_denominator
}

fn prime_factors( val: &usize ) -> HashMap<usize, isize> {
  let mut a_sqrt = 0;
  while a_sqrt*a_sqrt <= *val {
    a_sqrt += 1;
  }

  let mut num = *val;
  //let mut p_fac: Vec<usize> = Vec::new();
  let mut p_fac = HashMap::new();
  while num % 2 == 0 { 
    let factor = p_fac.entry(2).or_insert(0);
    *factor += 1;
    num = num / 2;
  }
  for i in (3..a_sqrt+1).step_by(2) {
    while num % i == 0 { 
      let factor = p_fac.entry(i).or_insert(0);
      *factor += 1;
      num = num / i;
    }   
  }
  if num != 1 { 
    p_fac.entry(num).or_insert(1);
  }
  p_fac
}


#[derive(Debug)]
struct RationalFraction {
  n: usize,
  d: usize,
  val: f64,
}

impl RationalFraction {
  fn new( n: usize, d: usize ) -> RationalFraction {
    RationalFraction {
      n: n,
      d: d,
      val: (n as f64)/(d as f64),
    }
  }

  fn double_zeroes(&self) -> bool {
    self.n%10 == 0 && self.d%10 == 0
  }

  fn incorrect_cancellation(&self) -> bool {
    let n_digit1 = self.n/10;
    let n_digit2 = self.n%10;
    let d_digit1 = self.d/10;
    let d_digit2 = self.d%10;
    let reduced_n: usize;
    let reduced_d: usize;
    if n_digit1 == d_digit1 {
      reduced_n = n_digit2;
      reduced_d = d_digit2;
    } else if n_digit1 == d_digit2 {
      reduced_n = n_digit2;
      reduced_d = d_digit1;
    } else if n_digit2 == d_digit1 {
      reduced_n = n_digit1;
      reduced_d = d_digit2;
    } else if n_digit2 == d_digit2 {
      reduced_n = n_digit1;
      reduced_d = d_digit1;
    } else {
      reduced_n = 200;
      reduced_d = 100;
    }
    let bad_reduction = RationalFraction::new( reduced_n, reduced_d );
    bad_reduction.val == self.val
  }
}
//mod prob_33;
//fn main() {
//    println!( "Problem 33\n  {:?} -> {}\n", "N/A", prob_33::solution() );
//}
