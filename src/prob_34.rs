///Project Euler Problem 34
///
///   145 is a curious number as 1! + 4! + 5! = 1 + 24 + 120 = 145
///   Find the sum of all numbers which are equal to the sum of the 
///   factorial of their digits
///
///   Note - 1! = 1 and 2! = 2 are not sums and thus excluded
pub fn solution() -> usize {
  let u_lim = 2540160; //sum of 7 digit num maxes out here
  let factorials = vec![1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880];
  let mut factorial_sum = 0;

  for i in 10..u_lim {
    let mut val = i;
    let mut sum = 0;
    while val > 0 {
      sum += factorials[val%10];
      val = val / 10;
    }
    if sum == i {
      factorial_sum += sum;
    }
  }
  factorial_sum
}
//mod prob_34;
//fn main() {
//    println!("Problem 34\n  {:?} -> {}\n", "N/A", prob_34::solution());
//}
