///Project Euler 35
///
///   197 is a circular prime because all rotations of the digits are prime
///      197, 971, 719
///
///   There are 13 such primes below 100: 
///     2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, 97
///
///   How many circular primes are there below 1,000,000?

pub fn solution( max: usize ) -> usize {
  let mut circular_count = 0;
  let primes = sieve_of_eratosthenes( max - 1 );
  for i in primes {
    let mut all_prime = true;
    let values = rotate( i );
    //println!("{} -> {:?}", i, values);
    for val in values {
      if !is_prime(val) {
        all_prime = false;
      }
    }
    if all_prime {
      //println!("{}", i);
      circular_count += 1;
    }
    //println!("");
  }
  circular_count
}

fn rotate( mut val: usize ) -> Vec<usize> {
  let mut digits = Vec::new();
  while val > 0 {
    digits.insert( 0, val % 10 );
    val /= 10;
  } 
  //println!( "{:?}", digits );
  let mut rotations = Vec::new();
  for index in 0..digits.len() {
    let mut oom = 0;
    let mut rotation = 0;
    for x in (0..index).rev() {
      rotation += digits[x]*10_usize.pow(oom as u32);
      oom += 1;
    }
    for x in (index..digits.len()).rev() {
      rotation += digits[x]*10_usize.pow(oom as u32);
      oom += 1;
    }
    rotations.push(rotation);
  }
  rotations
}

fn is_prime( val: usize ) -> bool {
  let mut a_sqrt = 0;
  while a_sqrt*a_sqrt <= val {
    a_sqrt += 1;
  }
  for i in 2..a_sqrt {
    if val % i == 0 {
      return false;
    }
  }
  true
}

fn sieve_of_eratosthenes( n: usize ) -> Vec<usize> {
  let mut nums: Vec<bool> = Vec::new();
  for _ in 0..n {
    nums.push(true);
  }
  let mut p = 2;  
  while p*p < nums.len()+1 {
    for i in (2*p..nums.len()).step_by(p) {
      //println!("Bad candidate -> {}", i);
      nums[i] = false;
    }   
    p += 1;
  }
  let mut primes: Vec<usize> = Vec::new();
  for i in 2..nums.len() {
    if nums[i] == true {
      primes.push(i);
    }   
  }
  primes
}

#[test]
fn simple_test() {
  assert_eq!( solution(100), 13 );
}
