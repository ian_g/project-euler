///Project Euler Problem 36
///
///   The decimal number 585 is 1001001001 in binary and palindromic in both
///   Find the sum of all numbers under 1000000 which are palindromic in both
///   base 10 and base 2
///   NOTE: The palindromic number may not include leading zeroes

//NOTE - 2 registers as palindromic in binary by this function. Dunno why, it's
//       the only even number that does. Sequence skips evens tho so it's fine
pub fn solution( max: usize ) -> usize {
  let mut max_binary_digits = 0;
  let mut palindromic_sum = 0;

  while 2_usize.pow(max_binary_digits) < max {
    max_binary_digits += 1;
  }
  for num in (1..max).step_by(2) { //even numbers end in 0 so not palindromic
    let bin_digits = to_binary( &num, max_binary_digits );  //in base 2
    let dec_digits = to_digits( &num );

    //Check palindromic
    let mut bin_pal = true;
    let mut dec_pal = true;
    for i in 0..bin_digits.len() {
      if bin_digits[i] != bin_digits[bin_digits.len()-i-1] {
        bin_pal = false;
      }
    }
    for i in 0..dec_digits.len() {
      if dec_digits[i] != dec_digits[dec_digits.len()-i-1] {
        dec_pal = false;
      }
    }

    //Add to sum
    if dec_pal && bin_pal {
      //println!( "{}", num );
      palindromic_sum += num;
    }
  }
  palindromic_sum
}

fn to_digits( num: &usize ) -> Vec<usize> {
  let mut digits = Vec::new();
  let mut val = *num;
  while val > 0 {
    digits.push(val%10);
    val /= 10;
  }
  digits
}

fn to_binary( num: &usize, mut max_digits: u32 ) -> Vec<bool> { //sorta
  let mut digits = Vec::new();
  let mut val = *num;
  while max_digits > 0 {
    if 2_usize.pow(max_digits) < val {
      digits.push(true);
      val -= 2_usize.pow(max_digits);
    } else {
      if digits.len() != 0 {
        digits.push(false);
      } //No leading zeroes
    }
    max_digits -= 1;
  }
  if val == 1 {
    digits.push(true);
  } else {
    digits.push(false);
  }
  digits
}
