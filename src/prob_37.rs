///Project Euler Problem 37
///
///   The number 3797 has an interesting property.  Being prime, it is possible
///   to continuously remove digits left to right and remain prime at each
///   stage: 3797, 797, 97, 7.
///   Similarly, this works right to left: 3797, 379, 37, 3.
///
///   Find the sum of the only eleven primes that are truncatable from left to
///   right and right to left
///
///   NOTE - 2, 3, 5, and 7 are not truncatable primes

///Maximum found with slower version of solution
pub fn solution( max: usize ) -> usize {
  //let mut truncatable_primes = 0;
  let mut truncatable_sum = 0;
  //let mut num = 11;
  let primes = sieve_of_eratosthenes( max );
  for num in primes {
  //while truncatable_primes < 11 {
    if num < 10 {
      continue;
    }
    //if is_prime(&num) {
      if is_truncatable(&num) {
        //truncatable_primes += 1;
        truncatable_sum += num;
        //println!( "{}", num );
      }
    //}
  //  num += 2;
  }
  truncatable_sum
}

fn is_truncatable( num: &usize ) -> bool {
  //println!( "{}", num );
  let mut val = *num / 10;
  while val > 0 {
    //println!( "  {}", val );
    if val == 1 {
      return false;
    }
    if is_prime(&val) {
      val /= 10;
    } else {
      return false;
    }
  }
  let mut oom = 0;
  val = *num;
  while 10_usize.pow(oom) < val {
    oom += 1;
  }
  //println!( "  val -> {}, oom -> {}", val, oom );
  val = *num % 10_usize.pow(oom);
  while val > 0 {
    //println!( "  {}", val );
    if val == 1 {
      return false;
    }
    if is_prime(&val) {
      oom -= 1;
      val %= 10_usize.pow(oom);
    } else {
      return false;
    }
  }
  true
}

fn is_prime( num: &usize ) -> bool {
  if *num == 2 {
    return true;
  }
  let mut a_sqrt = 0;
  while a_sqrt*a_sqrt <= *num {
    a_sqrt += 1;
  }
  let mut prime = true;
  for i in 2..a_sqrt+1 {
    if num % i == 0 { prime = false;  }
  } 
  prime
}

fn sieve_of_eratosthenes( n: usize ) -> Vec<usize> {
  let mut nums: Vec<bool> = Vec::new();
  for _ in 0..n+1 {
    nums.push(true);
  }
  let mut p = 2; 
  while p*p < nums.len()+1 {
    for i in (2*p..nums.len()).step_by(p) {
      //println!("Bad candidate -> {}", i);
      nums[i] = false;
    }
    p += 1;
  }
  let mut primes: Vec<usize> = Vec::new();
  for i in 2..nums.len() {
    if nums[i] == true {
      primes.push(i);
    }
  }
  primes
}
//mod prob_37;
//fn main() {
//    let val = 1000000;
//    println!( "Problem 36\n  {:?} -> {}\n", val, prob_37::solution(val) );
//}
