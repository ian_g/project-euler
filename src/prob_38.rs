///Project Euler Problem 38
///
///   Take 192 and multiply it by each of 1, 2, and 3
///     192 * 1 = 192
///     192 * 2 = 384
///     192 * 3 = 576
///
///   By concatenating each of the three products we get a pandigital number
///   192384576.  We'll call that the concatenated product of 192 and (1,2,3)
///
///   The same can be achieved by starting with 9 and miltiplying by 1, 2, 3, 
///   4, and 5 giving 918273645 - the concatenated sum of 9 and (1,2,3,4,5).
///
///   What is the largest 1 to 9 pandigital 9-digit number that can be formed
///   as the concatenated product of an ingeger with (1,2,...,n) where n > 1?

//main.rs, max set to 10000 b/c larger than that and concatenated sum too long
pub fn solution( max: usize ) -> usize {
  let mut largest_concatenated_sum = 0;
  for i in 1..max {
    //println!( "{}", i );
    let mut digits: Vec<usize> = Vec::new();
    let mut mult = 1;
    while digits.len() < 9 {
      let insert_locn = digits.len();
      let mut val = i*mult;
      while val > 0 {
        digits.insert( insert_locn, val%10 );
        val /= 10;
      }
      mult += 1;
    }
    //println!( "{:?}", digits );
    if digits.len() != 9 {
      continue;
    }
    if !is_pandigital( &digits ) {
      continue;
    }
    let mut value = 0;
    for digit in digits {
      value *= 10;
      value += digit;
    }
    if value > largest_concatenated_sum {
      //println!( "{} -> {}", i, value );
      largest_concatenated_sum = value;
    } //else {
    //  println!( "  {} -> {}", i, value );
    //}
  }
  largest_concatenated_sum
}

fn is_pandigital( digits: &Vec<usize> ) -> bool {
  let mut present = vec![false, false, false, false, false, false, false, false, false, false];
  for val in digits {
    present[*val] = true;
  }
  for index in 1..10 {
    if present[index] == false {
      return false;
    }
  }
  true
}
//mod prob_38;
//fn main() {
//    let val = 10000;
//    println!( "Problem 38\n  {:?} -> {}\n", val, prob_38::solution(val) );
//}
