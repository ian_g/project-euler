///Project Euler Problem 39
///
///   If p is the perimiter of a right triangle with integral length sides
///   {a, b, c}, there are exactly three solutions for p = 120.
///
///     {20, 48, 52}; {24, 45, 51}; {30, 40, 50}
///
///   For which value of p <= 1000 is the number of solutions maximized?
///
pub fn solution( max: usize ) -> usize {
  //Triangle inequality: for a triangle with sides x, y, z
  //   x < y + z
  //   y < x + z
  //   z < x + y
  let mut c: usize;
  let mut solutions: usize;
  let mut max_solutions: usize = 0;
  let mut max_solutions_value = 0;
  for perim in 3..max {
    solutions = 0;
    for a in 0..perim-2 { //Smallest
      for b in a..perim-a-1 { //Middle
        if a + b >= perim {
          continue;
        }
        c = perim-a-b;  //Largest
        if a >= b + c || b >= a + c || c >= a + b {
          continue;
        }
        if c < a || c < b {
          continue;
        }
        if a*a + b*b == c*c {
          solutions += 1; 
        }
      }
    }
    if solutions > max_solutions {
      //println!( "{} -> {}", perim, solutions );
      max_solutions_value = perim;
      max_solutions = solutions;
    }
  }
  max_solutions_value
}
//mod prob_39;
//fn main() {
//    let val = 1000;
//    println!( "Problem 39\n  {:?} -> {}\n", val, prob_39::solution(val) );
//}
