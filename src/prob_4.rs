///Solution function for Project Euler 4
///  Problem - Find the largest palindrome created as a product of two n-digit numbers
///  Palindromic Number - reads identically both ways.  E.G. 9009 = 99x91
pub fn solution( digits: u64 ) -> u64 {
  let mut largest = 0;
  let max = maximum( digits );
  for i in 1..max+1 {
    for j in 1..max+1 {
      if is_palindrome( &i, &j ) && i*j > largest {
        largest = i*j;
      }
    }
  }
  largest
}

fn maximum( digits: u64 ) -> u64 {
  let mut max = 0;
  for _i in 0..digits {
    max = max*10 + 9
  }
  max
}

fn is_palindrome( i: &u64, j: &u64 ) -> bool {
  let num = *i * *j;
  let rev = reverse( &num );
  if num == rev { 
    true
  } else {
    false
  }
}

fn reverse( num: &u64 ) -> u64 {
  let mut num2 = *num;
  let mut rev = 0;
  while num2 > 0 {
    rev = rev*10 + num2 % 10;
    num2 = num2 / 10;
  }
  rev
}

#[test]
fn simple_test() {
  assert_eq!( solution(2), 9009 );
}
