///Solution function for Project Euler 5
///  Problem - What is the smallest positive number evenly divisible by all numbers from 1 to a given value?
use std::collections::HashMap;

pub fn solution( max: u64 ) -> u64 {
  let mut p_facs: Vec<Vec<u64>> = Vec::new();
  for mut i in 1..max+1 {
    let a_sqrt = approx_sqrt( &i );
    p_facs.push( prime_factors(&mut i, &a_sqrt) )
  }

  let mut common_facs = HashMap::new(); 
  //Iterate over all the prime factorizations of the successive number
  for i in 0..max {
    let mut facs = HashMap::new();
    let val_facs = &p_facs[i as usize]; 
  
    for j in val_facs { //No need to &p_facs because already a reference
      let count = facs.entry(j).or_insert(0);
      *count += 1;
    }
  
    for (factor, count) in &facs {
      //Get one of each unique element e.g. for 20 - [11, 13, 17, 19]
      //Get enough of each common element e.g. for 20 - [ 2, 2, 2, 2, 3, 3, 5, 7 ]
      if common_facs.get(factor) == None {
        common_facs.insert(*factor, *count); //Add if missing
      } else if *common_facs.get(factor).unwrap() < *count {
        common_facs.insert(*factor, *count); //Overwrite if larger
      }
    }
  }

  let mut result: u64 = 1;
  //println!("Hashmap -> {:?}", common_facs);
  for (factor, count) in &common_facs {
    //let tmp = result;
    for _i in 0..*count {
      result *= *factor;
    }
    //println!("{} -> {}", tmp, result);
  }
  result
}

fn prime_factors( num: &mut u64, a_sqrt: &u64  ) -> Vec<u64> {
  let mut p_fac: Vec<u64> = Vec::new();
  while *num % 2 == 0 {
    p_fac.push( 2 );
    *num = *num / 2;
  }
  for i in (3..*a_sqrt+1).step_by(2) {
    if *num % i == 0 { 
      p_fac.push( i );
      *num = *num / i;
    }
  }
  if *num != 1 {
    p_fac.push( *num )
  }
  p_fac
}

fn approx_sqrt( num: &u64 ) -> u64 {
  let mut guess = 1;
  while guess*guess < *num {
    guess += 1;
  }
  guess
}

#[test]
fn simple_test() {
  assert_eq!( solution(10), 2520 );
}
