///Solution function for Project Euler 6
///  Problem - Find the difference between the sum of the squares and the square of the sum of the specified natural numbers
///  Example - 1^2 + 2^2 + ... + 10^2 = 385
///          - (1 + 2 + ... + 10)^2 = 3025
///          -  3025 - 385 = 2640
pub fn solution( max: u64 ) -> u64 {
  let mut sum_of_squares = 0;
  let mut sum = 0;
  for i in 1..max+1 {
    sum_of_squares += i*i;
    sum += i;
  }
  let square_of_sums = sum*sum;
  square_of_sums - sum_of_squares
}

#[test]
fn simple_test() {
  assert_eq!( solution(10), 2640 );
}
