///Solution function for Project Euler 7
///  Problem - Find the nth prime number 
///  Example - 2, 3, 5, 7, 11, 13  are the first six primes
pub fn solution( max: u64 ) -> u64 {
  //Starts like this because immediately goes to 3 in while loop
  //And if didn't immediately do that, approx_sqrt gives 1 as its 
  //  approximation which doesn't work
  let mut count: u64 = 1;  
  let mut value: u64 = 2;
  while count < max {
    value += 1;
    let a_sqrt = approx_sqrt( &value );
    if is_prime( &value, &a_sqrt ) {
      //println!("Prime -> {}", value );
      count += 1;
    }
  }
  value
}

fn is_prime( num: &u64, a_sqrt: &u64 ) -> bool {
  let mut prime = true;
  for i in 2..a_sqrt+1 {
    if num % i == 0 { prime = false;  }
  } 
  prime
}

fn approx_sqrt( num: &u64 ) -> u64 {
  let mut guess = 1;
  while guess*guess < *num {
    guess += 1;
  }
  guess
}

#[test]
fn simple_test() {
  assert_eq!( solution(6), 13 );
}
