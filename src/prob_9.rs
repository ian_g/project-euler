///Solution function for Project Euler 9
///  Problem - Find a Pythagorean triplet for which a + b + c = N.  Sometimes there is only one solution, like when N=1000.  Return the product a*b*c 
///  Example - a = 3, b = 4, c = 5 - a^2 + b^2 = c^2 - a * b * c = 3 * 4 * 5 = 60 
pub fn solution( sum: u64 ) -> u64 {
  let mut product: u64 = 0;
  for a in 1..sum {
    for b in 1..(sum-a-1) { //Can't exceed sum.  Need 1 for c at least
      let c = sum - a - b;
      //Triangle inequality - sum of lengths of any two sides must be greater than or equal to length of remaining side
      if a + b < c || a + c < b || b + c < a {
        ();
      } else {
        let sides = allocate_values( &a, &b, &c );
        if is_pythagorean( sides.2, sides.1, sides.0 ) {
          product = a*b*c;
        }
      } 
    }
  }
  product
}

fn allocate_values( a: &u64, b: &u64, c: &u64 ) -> (u64, u64, u64) {
  if *a >= *b && *a >= *c {
    return ( *a, *b, *c );
  } else if *b >= *a && *b >= *c {
    return ( *b, *a, *c );
  } 
  return ( *c, *a, *b )
}

fn is_pythagorean( a: u64, b: u64, c: u64 ) -> bool {
  if a*a + b*b == c*c { 
    return true;
  }
  false
}

#[test]
fn simple_test() {
  assert_eq!( solution(12), 60 );
}
